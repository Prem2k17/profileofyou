import { Component,ViewChild } from '@angular/core';

import { Camera, CameraOptions, CameraPopoverOptions } from '@ionic-native/camera/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('fileInput') fileInput;
  options: CameraOptions;
  cameraPopOverOptions:CameraPopoverOptions;
  selectedImage:any = "/assets/imgs/profile.png";

  profileData:any = {
    "name":"",
    "phone":null,
    "skills":"",
    "qualification":"",
    "profilePic":""
  };


  constructor(private camera: Camera,private androidPermissions: AndroidPermissions) {
    this.options = {
      quality: 50,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      popoverOptions: this.cameraPopOverOptions,
      saveToPhotoAlbum: false
    }
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
      result => {/* alert('Has permission?'+result.hasPermission) */},
      err => {
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA).then(res=>{
       // alert("User permission:: "+res);
      },err=>{
       // alert("User permission err:: "+err);
      })}
    );
    
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA]);
  }

  takePicture() {
    try{
    //if (Camera['installed']()) {
      this.camera.getPicture(this.options).then((data) => {
        let base64Image = 'data:image/jpeg;base64,' + data;
       // alert(base64Image);
      }, (err) => {
        alert('Unable to take photo');
      })
   /// }
    /*  else {
      alert("browser fileInput");
      this.fileInput.nativeElement.click();
    } */
  }
  catch(e){
    this.fileInput.nativeElement.click();
  }


   /*  this.camera.getPicture(this.options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      alert(base64Image);
    }, (err) => {
      // Handle error
      alert("err::"+err)
    }); */
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.selectedImage = imageData;
     // alert(imageData);
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  submitData(){
    alert("Data Submitted Successfully..")
  }

}
